package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

@DisplayName("Usability unit tests")
public class appSpec {

    @Test
    public void ThousandsNeedsApproval() {
        Main app = new Main();
        boolean res = app.approval("990");
        assertTrue(res,() -> "990 needs approval");
    }

    @Test
    public void FileHundredsDoesNotNeedApproval() {
        Main app = new Main();
        boolean res = app.approval("500");
        assertFalse(res,() -> "500 does not need approval");
    }

}
